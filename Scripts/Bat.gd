extends KinematicBody2D

const DEATH_EFFECT_PATH = "res://Assets/Effects/EnemyDeathEffect.tscn"

const EnemyDeathEffect = preload(DEATH_EFFECT_PATH)

export var ACCELERATION = 300
export var MAX_SPEED = 50
export var FRICTION = 200
export var WANDER_RANGE = 4


enum {
	IDLE,
	WANDER,
	CHASE
}

var knockback = Vector2.ZERO
var velocity = Vector2.ZERO


var state = IDLE

onready var sprite = $AnimatedSprite
onready var stats = $Stats
onready var playerDetectionZone = $PlayerDetectionZone
onready var hurtbox = $HurtBox
onready var softColission = $SoftCollision
onready var wanderController = $WanderControler
onready var animationPlayer = $AnimationPlayer

func _ready(): 
	state = pick_random_state([IDLE, WANDER])

func _physics_process(delta):
	knockback = knockback.move_toward(Vector2.ZERO, FRICTION * delta)
	knockback = move_and_slide(knockback)

	match state:
		IDLE:
			velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
			seek_player()
			if wanderController.get_time_left() == 0:
				update_wander()

		WANDER:
			seek_player()
			if wanderController.get_time_left() == 0:
				update_wander()
			accelerate_towards_point(wanderController.target_position, delta)
			if global_position.distance_to(wanderController.target_position) <= WANDER_RANGE:
				update_wander()

		CHASE:
			var player = playerDetectionZone.player
			if player != null:
				# Normalize the distance from the bat to the player and allow the approach speed to be
				# more natural as opposed to closing the distance
				# Normalize helps us set the interval
				accelerate_towards_point(player.global_position, delta)
				
			else:
				state = IDLE

	if softColission.is_colliding():
		velocity += softColission.get_push_vector() * delta * 400
	velocity = move_and_slide(velocity)


func accelerate_towards_point(position, delta):
	var distanceInterval = global_position.direction_to(position)
	velocity = velocity.move_toward(distanceInterval * MAX_SPEED, ACCELERATION * delta)
	sprite.flip_h = velocity.x < 0


func seek_player():
	if playerDetectionZone.can_see_player():
		state = CHASE


func update_wander():
	state = pick_random_state([IDLE, WANDER])
	wanderController.start_wander_timer()


func pick_random_state(state_list):
	state_list.shuffle()
	return state_list.pop_front()


func _on_HurtBox_area_entered(area):
	stats.health -= area.damage
	knockback = area.knockback_vector * 125
	hurtbox.create_hit_effect()
	hurtbox.start_invincibility(0.4)


func _on_Stats_no_health():
	queue_free()
	var enemyDeathEffect = EnemyDeathEffect.instance();
	get_parent().add_child(enemyDeathEffect)
	enemyDeathEffect.global_position = global_position


func _on_HurtBox_invincibility_started():
	animationPlayer.play("Start")


func _on_HurtBox_invincibility_ended():
	animationPlayer.play("Stop")
