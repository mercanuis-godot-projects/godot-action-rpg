extends KinematicBody2D

const PlayerHurtSound = preload("res://Assets/Player/PlayerHurtSound.tscn")

# Figures based on testing on M1 DEBUG
# Might need to be adjusted depending on system
const ACCELERATION = 500
const FRICTION = 500
const MAX_SPEED = 80
const ROLL_SPEED = 125



# Points to parameters used by the player
const PLAYBACK_STATE = "parameters/playback"
const IDLE_BLEND_POSITION = "parameters/Idle/blend_position"
const RUN_BLEND_POSITION = "parameters/Run/blend_position"
const ATTACK_BLEND_POSTIION = "parameters/Attack/blend_position"
const ROLL_BLEND_POSTIION = "parameters/Roll/blend_position"

enum {
	MOVE, ROLL, ATTACK
}

var state = MOVE
var velocity = Vector2.ZERO
var roll_vector = Vector2.DOWN
var stats = PlayerStats

onready var animationPlayer = $AnimationPlayer
onready var animationTree = $AnimationTree
onready var animationState = animationTree.get(PLAYBACK_STATE)
onready var sword_hitbox = $HitboxPivot/SwordHitbox
onready var hurtbox = $HurtBox
onready var blinkAnimationPlayer = $BlinkAnimationPlayer

func _ready():
	randomize()
	stats.connect("no_health", self, "queue_free")
	animationTree.active = true
	sword_hitbox.knockback_vector = roll_vector


func _physics_process(delta):
	match state:
		MOVE:
			move_state(delta)
		ATTACK:
			attack_state()
		ROLL:
			roll_state()
	
	
func move_state(delta):
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	
	# Allows for the vector velocity to be the same (helps with diagonal movement)
	input_vector = input_vector.normalized()
	
	# Using delta, ties the frame rate to the current performance of the CPU
	# Allows the movement to be more smooth
	# Friction and Accelleration allow for the sprite to speed up and slow down
	if input_vector != Vector2.ZERO:
		roll_vector = input_vector
		sword_hitbox.knockback_vector = input_vector
		
		animationTree.set(IDLE_BLEND_POSITION, input_vector)
		animationTree.set(RUN_BLEND_POSITION, input_vector)
		animationTree.set(ATTACK_BLEND_POSTIION, input_vector)
		animationTree.set(ROLL_BLEND_POSTIION, input_vector)
		
		animationState.travel("Run")
		velocity = velocity.move_toward(input_vector * MAX_SPEED, ACCELERATION * delta)
	else:
		animationState.travel("Idle")
		velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
		
	move()
	
	if Input.is_action_just_pressed("attack"):
		state = ATTACK
	if Input.is_action_just_pressed("roll"):
		state = ROLL
	
	
func attack_state():
	velocity = Vector2.ZERO
	animationState.travel("Attack")
	
	
func attack_done():
	state = MOVE
	
	
func roll_state():
	velocity = roll_vector * ROLL_SPEED
	animationState.travel("Roll")
	move()
	
	
func roll_done():
	velocity = velocity * 0.8
	state = MOVE
	
	
func move():
	velocity = move_and_slide(velocity)


func _on_HurtBox_area_entered(area):
	stats.health -= area.damage
	hurtbox.start_invincibility(0.6)
	hurtbox.create_hit_effect()
	var playerHurtSound = PlayerHurtSound.instance()
	get_tree().current_scene.add_child(playerHurtSound)


func _on_HurtBox_invincibility_started():
	blinkAnimationPlayer.play("Start")


func _on_HurtBox_invincibility_ended():
	blinkAnimationPlayer.play("Stop")
