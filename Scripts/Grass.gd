extends Sprite

const GRASS_EFFECT_PATH = "res://Assets/Effects/GrassEffect.tscn"
const GrassEffect = preload(GRASS_EFFECT_PATH)

func create_grass_effect():
	var grassEffect = GrassEffect.instance()
	get_parent().add_child(grassEffect)
	grassEffect.global_position = global_position
	
	
func _on_HurtBox_area_entered(_area):
	create_grass_effect()
	queue_free()
